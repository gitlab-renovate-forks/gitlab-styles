# frozen_string_literal: true

require_relative '../../../gitlab/styles/common/banned_constants'

module Rubocop
  module Cop
    module Fips
      # Flags usage of the Digest class (which is not FIPS-compliant) and suggests replacing it with OpenSSL::Digest
      # (which is FIPS-compliant).
      #
      # @example
      #   # bad
      #   Digest::SHA1.hexdigest('foo')
      #   Digest::SHA512('foo')
      #
      #   # good
      #   OpenSSL::Digest::SHA1.hexdigest('foo')
      #   OpenSSL::Digest::SHA512.hexdigest('foo')
      class OpenSSL < RuboCop::Cop::Base
        extend RuboCop::Cop::AutoCorrector
        include Gitlab::Styles::Common::BannedConstants

        MESSAGE_TEMPLATE = 'Usage of this class is not FIPS-compliant. Use %{replacement} instead.'

        REPLACEMENTS = {
          'Digest::SHA1' => 'OpenSSL::Digest::SHA1',
          'Digest::SHA2' => 'OpenSSL::Digest::SHA256',
          'Digest::SHA256' => 'OpenSSL::Digest::SHA256',
          'Digest::SHA384' => 'OpenSSL::Digest::SHA384',
          'Digest::SHA512' => 'OpenSSL::Digest::SHA512'
        }.freeze

        def initialize(config = nil, options = nil)
          @message_template = MESSAGE_TEMPLATE
          @replacements = REPLACEMENTS
          @autocorrect = true
          super
        end
      end
    end
  end
end
