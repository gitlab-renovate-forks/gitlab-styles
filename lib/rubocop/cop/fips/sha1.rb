# frozen_string_literal: true

require_relative '../../../gitlab/styles/common/banned_constants'

module Rubocop
  module Cop
    module Fips
      # Cop that denies the use of Digest::SHA1
      # @example
      #   # bad
      #   OpenSSL::Digest::SHA1.hexdigest('foo')
      #   Digest::SHA1('ABC')
      #
      #   # good
      #   OpenSSL::Digest::SHA256.hexdigest('foo')
      class SHA1 < RuboCop::Cop::Base
        include Gitlab::Styles::Common::BannedConstants

        MESSAGE_TEMPLATE = 'SHA1 is likely to become non-compliant in the near future. Use %{replacement} instead.'

        REPLACEMENTS = {
          'OpenSSL::Digest::SHA1' => 'OpenSSL::Digest::SHA256',
          'Digest::SHA1' => 'OpenSSL::Digest::SHA256'
        }.freeze

        def initialize(config = nil, options = nil)
          @message_template = MESSAGE_TEMPLATE
          @replacements = REPLACEMENTS
          @autocorrect = false
          super
        end
      end
    end
  end
end
