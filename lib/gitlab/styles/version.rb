# frozen_string_literal: true

module Gitlab
  module Styles
    VERSION = '13.1.0'
  end
end
