# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../../lib/rubocop/cop/rspec/rails_controller_testing'

RSpec.describe Rubocop::Cop::RSpec::RailsControllerTesting do
  shared_examples 'controller test methods' do |test_type|
    it 'ignores response body assertions' do
      expect_no_offenses(<<~RUBY, "spec/#{test_type}/foo_spec.rb")
        expect(response.status).to eq(200)
        expect(response.body).to eq('OK')
      RUBY
    end

    it "registers an offense when using assigns in a #{test_type} spec" do
      expect_offense(<<~RUBY, "spec/#{test_type}/foo_spec.rb")
        expect(assigns[:search_error_if_version_incompatible]).to be_falsey
               ^^^^^^^ Avoid using `assigns`. Instead, assert on observable outputs such as the response body. See https://docs.gitlab.com/ee/development/testing_guide/testing_levels.html#about-controller-tests
      RUBY
    end

    it "registers an offense when using render_template in a #{test_type} spec" do
      expect_offense(<<~RUBY, "spec/#{test_type}/foo_spec.rb")
        expect(assigns[:search_error_if_version_incompatible]).to be_falsey
               ^^^^^^^ Avoid using `assigns`. Instead, assert on observable outputs such as the response body. See https://docs.gitlab.com/ee/development/testing_guide/testing_levels.html#about-controller-tests
      RUBY
    end
  end

  it_behaves_like('controller test methods', 'controllers')
  it_behaves_like('controller test methods', 'requests')
end
